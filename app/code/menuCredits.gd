extends Control

var counter = 0
var credits = [
	"Zombies Go",
	"A Game Jam Project",
	"Manager\nKiraid",
	#"Tool Selection\nDaddySmash\nKalietha",
	"Programer\nKiraid\nDaddySmash",
	"Visual Artist\nShadua",
	"Composer\nKalietha",
	#"Story\nXXXX",
	"Playtester\nDaddySmash\nRei Shards",
	""
	]

func _ready():
	get_node("/root/global").credits = credits
	counter = credits.size() - 1

func next_credit():
	if (counter < credits.size() - 1):
		counter += 1
	else:
		counter = 0
	get_node("text").set_text(credits[counter])