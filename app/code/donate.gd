extends Node

var text = [
	"",
	"",
	"Play at Website:",
	"zombiesgo.gitlab.io",
	"Donate to the Team:",
	"cash.me/RefinedSoftwareLLC",
	"Wiki & Source Code:",
	"gitlab.com/zombiesgo/zombiesgo.gitlab.io/wikis/home"
	]

func _ready():
	get_tree().set_auto_accept_quit(false) #Enables: _notification(what) to recieve MainLoop.NOTIFICATION_WM_QUIT_REQUEST
	var t = ""
	#for i in range(get_node("/root/global").credits.size()):
	#	t += get_node("/root/global").credits[i] + "\n"
	for i in range(text.size()):
		t += text[i] + "\n"
	print(t)
	get_node("text").set_text(t)

func _notification(what):
	if (what==MainLoop.NOTIFICATION_WM_QUIT_REQUEST):
		get_node("/root/global").enterMenu()

func _on_backGround_pressed():
	if get_node("/root/global").isInputEnabled():
		get_node("/root/global").enterMenu()