
#extends AnimatedSprite
extends TextureButton

var index = null

func getDamaged():
	if get_parent().get_parent().get_parent().dead:
		return
	
	index = get_parent().get_parent().get_parent().getIndexFromNode(self.get_parent())
	if(get_parent().get_parent().get_parent().zombieData[index].zombieFadeStart != 0): #Check to see if the zombie was already killed.
		return
	print("Get Damaged ", index)
	get_node("/root/global").audioZombie()
	#Start showing red damage on screen. Then fade it out.  If it is fading, then restart the fading animation.
	get_parent().get_parent().get_parent().get_node("recievingDamage").get_node("anim").play("damage")
	get_parent().get_parent().get_parent().get_node("recievingDamage").get_node("anim").seek(0, true)
	get_parent().get_parent().get_parent().get_node("recievingDamage").show()
	if (get_parent().get_parent().get_parent().get_node("mouse").get_node("animations").get_node("anim").get_current_animation() == "empty"):
		print("Death due to zombie.")
		get_parent().get_parent().get_parent().die()
		
	elif (get_parent().get_parent().get_parent().get_node("mouse").get_node("animations").get_node("anim").get_current_animation() == "held"):
		get_parent().get_parent().get_parent().get_node("mouse").get_node("animations").get_node("anim").play("breaking", float(-1), float((float(get_parent().get_parent().get_parent().playerDifficulty) + 8) / 12))
		print("Tomb was held.")
		
	elif (get_parent().get_parent().get_parent().get_node("mouse").get_node("animations").get_node("anim").get_current_animation() == "swinging"):
		get_parent().get_parent().get_parent().get_node("mouse").get_node("animations").get_node("anim").play("breaking", float(-1), float((float(get_parent().get_parent().get_parent().playerDifficulty) + 8) / 12))
		print("Tomb was swinging.")
		
	elif (get_parent().get_parent().get_parent().get_node("mouse").get_node("animations").get_node("anim").get_current_animation() == "breaking"): #If you are breaking, then you are immune to damage.
		print("Tomb was breaking.")
		
	else:
		print("Death due to error.")
		get_parent().get_parent().get_parent().die()


func stopMyAnimation(): #Stop the animation and restart the zombie time.
	if get_parent().get_parent().get_parent().dead:
		return
	index = get_parent().get_parent().get_parent().getIndexFromNode(self.get_parent())
	get_parent().get_parent().get_parent().zombieStart(index)


func _on_zombieHatHeadThrown_pressed():
	if get_parent().get_parent().get_parent().dead:
		return
	index = get_parent().get_parent().get_parent().getIndexFromNode(self.get_parent())
	print("Zombie Thrown Head Clicked ", index)
	if (get_parent().get_parent().get_parent().get_node("mouse").get_node("animations").get_node("anim").get_current_animation() != "breaking"
	and get_node("anim").is_playing()):
		if get_parent().get_parent().get_parent().zombieData[index].zombieFadeStart == 0:
			get_parent().get_parent().get_parent().zombieFadeStart(index)
			get_node("/root/global").audioHit()
			get_parent().get_parent().get_parent().playerScore += 1
