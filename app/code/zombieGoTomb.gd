var index = null

func _ready():
	#var b = BitMap.new()
	#b.create_from_image_alpha(get_node("zombieClip").get_node("tombClickMask").get_normal_texture().get_data())
	#get_node("zombieClip").get_node("tombClickMask").set_click_mask(b)
	#b = BitMap.new()
	#b.create_from_image_alpha(get_node("zombieClip").get_node("zombieClickMask").get_normal_texture().get_data())
	#get_node("zombieClip").get_node("zombieClickMask").set_click_mask(b)
	#b = BitMap.new()
	#b.create_from_image_alpha(get_node("zombieHatHeadThrown").get_normal_texture().get_data())
	#get_node("zombieHatHeadThrown").set_click_mask(b)
	pass

func _on_clickArea_pressed():
	if get_parent().get_parent().dead:
		return
	index = get_parent().get_parent().getIndexFromNode(self)
	#print("Zombie Clicked ", index)
	if (get_parent().get_parent().zombieData[index].zombieFadeStart == 0
	and get_parent().get_parent().playerTime > (get_parent().get_parent().zombieData[index].zombieStart + (get_parent().get_parent().zombieData[index].zombieTime * 0.1))
	and get_parent().get_parent().playerTime < (get_parent().get_parent().zombieData[index].zombieStart + get_parent().get_parent().zombieData[index].zombieTime)):
		get_parent().get_parent().zombieFadeStart(index)
		get_node("/root/global").audioHit()
		get_parent().get_parent().playerScore += 1

func _on_tombClickMask_pressed():
	if get_parent().get_parent().dead:
		return
	index = get_parent().get_parent().getIndexFromNode(self)
	#print("Tomb Clicked ", index)
	if (get_parent().get_parent().get_node("mouse").get_node("animations").get_node("anim").get_current_animation() == "empty"
		and get_node("normalTomb").is_visible()):
		get_node("normalTomb").hide() #We may need to change the data structure to hold the tombs new state.
		get_node("missingTomb").show()
		get_parent().get_parent().get_node("mouse").get_node("animations").mouseHeld()

func _on_clickGroundBlock_pressed():
	pass