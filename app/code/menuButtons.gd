extends HBoxContainer

var highArray = null
var highScoreTomb = null

func _ready():
	if OS.get_name() == "HTML5":
		remove_child(get_node("flee"))
		get_node("").set_alignment(ALIGN_CENTER)
	highArray = get_node("/root/global").highArray
	highScoreTomb = get_node("highScore")
	var highDifficulty = 0
	var highRank = 0
	for difficulty in range(get_node("/root/global").SIZE_DIFFICULTY):
		for rank in range(get_node("/root/global").SIZE_RANK):
			if highArray[difficulty][rank][get_node("/root/global").STAT_SCORE] == 0 or highArray[difficulty][rank][get_node("/root/global").STAT_TIME] == 0:
				pass
			elif difficulty > highDifficulty:
				highDifficulty = difficulty
				highRank = rank
			elif highArray[difficulty][rank][get_node("/root/global").STAT_SCORE] > highArray[highDifficulty][highRank][get_node("/root/global").STAT_SCORE]:
				highDifficulty = difficulty
				highRank = rank
			elif (highArray[difficulty][rank][get_node("/root/global").STAT_SCORE] == highArray[highDifficulty][highRank][get_node("/root/global").STAT_SCORE]
			and highArray[difficulty][rank][get_node("/root/global").STAT_TIME] > highArray[highDifficulty][highRank][get_node("/root/global").STAT_TIME]):
				highDifficulty = difficulty
				highRank = rank
	#if highArray[difficulty][rank][get_node("/root/global").STAT_NEW]:
	#	highScoreTomb.set_normal_texture(load("res://visual/atlasBloodyTomb.xatex"))
	highScoreTomb.get_child(0).get_node("name").set_text(str(highArray[highDifficulty][highRank][get_node("/root/global").STAT_NAME]))
	highScoreTomb.get_child(0).get_node("score").set_text(str(highArray[highDifficulty][highRank][get_node("/root/global").STAT_SCORE]))
	highScoreTomb.get_child(0).get_node("time").set_text(get_node("/root/global").timeString(highArray[highDifficulty][highRank][get_node("/root/global").STAT_TIME]))

func _draw():
	#VS.canvas_item_set_clip(get_canvas_item(), true)
	pass

func _on_flee_pressed():
	if get_node("/root/global").isInputEnabled():
		get_node("/root/global").enterOS()

func _on_highScore_pressed():
	if get_node("/root/global").isInputEnabled():
		get_node("/root/global").enterHighScore()

func _on_1_pressed():
	if get_node("/root/global").isInputEnabled():
		get_node("/root/global").enterRound(0)

func _on_2_pressed():
	if get_node("/root/global").isInputEnabled():
		get_node("/root/global").enterRound(1)

func _on_3_pressed():
	if get_node("/root/global").isInputEnabled():
		get_node("/root/global").enterRound(2)

func _on_4_pressed():
	if get_node("/root/global").isInputEnabled():
		get_node("/root/global").enterRound(3)

func _on_donate_pressed():
	if get_node("/root/global").isInputEnabled():
		get_node("/root/global").enterDonate()