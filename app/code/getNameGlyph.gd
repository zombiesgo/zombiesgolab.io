extends TextureButton

func _ready():
	pass

func _on_r_pressed():
	if get_node("/root/global").isInputEnabled():
		var char = get_child(0).get_child(0).get_text()
		print("THIS GLYPH WAS PRESSED! " + char)
		get_node("/root/global").disableInputForXMs(200)
		if char == "CLR":
			get_node("/root/global").currentName = ""
			get_parent().get_parent().get_node("DisplayPlayerNameOnScreen").set_text(get_node("/root/global").currentName)
		elif char == "END":
			if get_node("/root/global").currentName == "":
				get_node("/root/global").currentName = "RIP"
			get_node("/root/global").updateHighScorePart2()
		elif get_node("/root/global").currentName.length() >= 3:
			pass
		else:
			get_node("/root/global").currentName = get_node("/root/global").currentName + char
			get_parent().get_parent().get_node("DisplayPlayerNameOnScreen").set_text(get_node("/root/global").currentName)
			#print("Updated: " + get_node("/root/global").currentName)