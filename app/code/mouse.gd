extends AnimatedSprite

func _ready():
	set_process_input(true) #Enables: _input(InputEvent) to run every mouse change.
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	mouseEmpty()

func _input(ev):
	# Mouse in viewport coordinates. "ev" is an instance of the class InputEvent.
	if ev.type == InputEvent.MOUSE_BUTTON and ev.button_index == BUTTON_LEFT:
		pass
	elif ev.type == InputEvent.MOUSE_MOTION:
		#print("Mouse Motion at: ", ev.pos)
		get_parent().set_margin(MARGIN_TOP,  ev.pos.y)
		get_parent().set_margin(MARGIN_LEFT, ev.pos.x)

func mouseEmpty(): #This shows empty hands for the mouse.
	get_node("anim").play("empty")
	pass

func mouseHeld(): #This shows held tombstone for the mouse.
	get_node("anim").play("held")
	pass

func mouseBreaking(): #This shows tombstone breaking animation for the mouse.
	if (get_node("anim").get_current_animation() != "breaking"
		and get_node("anim").get_current_animation() != "empty"):
		get_node("anim").play("breaking")
	pass

func mouseSwinging(): #This shows tombstone swinging animation for the mouse.
	if (get_node("anim").get_current_animation() == "held"):
		get_node("anim").play("swinging")
	pass